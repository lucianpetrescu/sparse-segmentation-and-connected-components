#version 450

//kernel
layout(local_size_x = 32, local_size_y = 32) in;


//constants and uniforms
uniform int num_seeds;
const int NO_ID = num_seeds + 1;
uniform float THRESHOLD;
uniform float LENIENCY;

//binding 0
struct GPUSegmentationRegion {
	int seedx;						// seed x
	int seedy;						// seed y
	int id;							// region unique id
	int parentid;					// parent region id (should be equal to id by the time segmentation finishes working with it)
};
layout(std430, binding = 0) coherent buffer segmentation_regions_data
{
	GPUSegmentationRegion regions_data[];
};
//binding 1
struct GPUSegmentationRegionStats {
	int r, g, b;
	int samples;
};
layout(std430, binding = 1) coherent buffer segmentation_region_stats
{
	GPUSegmentationRegionStats region_merge_stats[];
};

layout(binding = 0) uniform sampler2D input_image;
layout(binding = 0, r32i) readonly uniform iimage2D input_regions;
layout(binding = 1, r8) readonly uniform image2D input_flux;
const int width = imageSize(input_regions).x;
const int height = imageSize(input_regions).y;


//----------------------------------------------------------------------------------------------------------------
//		HELPERS
//----------------------------------------------------------------------------------------------------------------
vec3 getColor(ivec2 pos){
	return texture(input_image, vec2(pos.x + 0.5, pos.y + 0.5) / vec2(width, height)).xyz;
}

//get greatest parent
int getGreatestParent(int regionid) {
	GPUSegmentationRegion region = regions_data[regionid];
	int id = region.id;
	int parentid = region.parentid;

	while (id != parentid) {
		//read new parent and id
		region = regions_data[parentid];
		id = region.id;
		parentid = region.parentid;
	}
	return id;
}

//----------------------------------------------------------------------------------------------------------------
//		IMPLEMENTATION
//----------------------------------------------------------------------------------------------------------------
void try_connect(int id1, int id2, ivec2 coord1, ivec2 coord2) {

	float e1 = imageLoad(input_flux, coord1).x;
	float e2 = imageLoad(input_flux, coord2).x; 
	if ( e1 >= 0.5 || e2 >= 0.5) return;	//only connect in a weak flux area

	if (id1 != NO_ID && id2 != NO_ID && id1 != id2) {
		//get parents
		int pid1 = getGreatestParent(id1);
		int pid2 = getGreatestParent(id2);

		//if different greatest ancestors -> try to connect
		if (pid1 != pid2 && id1 != pid2 && id2 != pid1 ) {

			//-------------------------------------------------------------------------------
			// this is a basic region grower, more sophisticated strategies can be implemented
			// knowledge of the input type also helps. E.g. depth segmentation is vastly different
			// from normals segmentation.
			//-------------------------------------------------------------------------------


			//-------------------------------------------------------------------------------
			//VERY STRICT NEIGHBOR REJECTION TESTING
			//-------------------------------------------------------------------------------
			//extremely strict local color test
			vec3 neighbor1 = getColor(coord1);
			vec3 neighbor2 = getColor(coord2);
			if(length(neighbor1 - neighbor2) > THRESHOLD * (LENIENCY) ) return;

			//-------------------------------------------------------------------------------
			// GLOBAL REJECTION TESTING
			//-------------------------------------------------------------------------------
			//
			GPUSegmentationRegionStats s1 = region_merge_stats[pid1];
			GPUSegmentationRegionStats s2 = region_merge_stats[pid2];
			vec3 region1 = vec3(s1.r / 1000.f, s1.g / 1000.f, s1.b / 1000.f );
			vec3 region2 = vec3(s2.r / 1000.f, s2.g / 1000.f, s2.b / 1000.f );
			if(length(region1 - region2) > THRESHOLD * (LENIENCY)) return;	

			//-------------------------------------------------------------------------------
			// CLUSTERD REJECTION TESTING
			//-------------------------------------------------------------------------------
			vec3 local1 = vec3(0,0,0);
			float count1 = 0;
			vec3 local2 = vec3(0,0,0);
			float count2 = 0;
			const int KERNEL = 5;
			for(int i =-KERNEL; i<=KERNEL; i++){
				for(int j =-KERNEL; j<=KERNEL; j++){
					ivec2 coord = ivec2(clamp(coord1.x + i, 0, width - 1), clamp(coord1.y + j, 0 , height - 1));
					int id = int(imageLoad(input_regions, coord).x);
					vec3 color = getColor(coord);
					if(id == id1){
						local1 += color;
						count1++;
					}else if(id == id2){
						local2 += color;
						count2++;
					}
				}
			}
			if(length(region1 - region2) > THRESHOLD * (LENIENCY)) return;	

			//-------------------------------------------------------------------------------
			//all rejection tests passed, time to connect objects
			//-------------------------------------------------------------------------------
			if(pid1 < pid2){
				atomicExchange(regions_data[pid2].parentid,  pid1);
			}else{
				atomicExchange(regions_data[pid1].parentid,  pid2);
			}

		}
	}
}

void main()
{
	//raw data
	int x = int(gl_GlobalInvocationID.x);
	int y = int(gl_GlobalInvocationID.y);

	//work
	if (x > 0 && y > 0 && x < width && y < height){
		
		ivec2 coord = ivec2(x,y);

		//get indices
		int id = int(imageLoad(input_regions, coord).x);
		if(id!=NO_ID){
			
			ivec2 coord_l = ivec2(clamp(x - 1, 0, width - 1), y);
			ivec2 coord_ld= ivec2(clamp(x - 1, 0, width - 1), clamp(y - 1, 0, height - 1));
			ivec2 coord_d = ivec2(x, clamp(y - 1, 0, height - 1));
			ivec2 coord_lt= ivec2(clamp(x - 1, 0, width - 1), clamp(y + 1, 0, height - 1));
			
			int id_l = int(imageLoad(input_regions, coord_l).x);
			int id_ld = int(imageLoad(input_regions, coord_ld).x);
			int id_d = int(imageLoad(input_regions, coord_d).x);
			int id_lt = int(imageLoad(input_regions, coord_lt).x);
		
			//try to connnect
			try_connect(id, id_l, coord, coord_l);
			try_connect(id, id_ld, coord, coord_ld);
			try_connect(id, id_d, coord, coord_d);
			try_connect(id, id_lt, coord, coord_lt);
		}
	}
}