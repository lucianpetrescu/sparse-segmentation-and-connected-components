#version 440
layout(location = 0) out vec4 out_color;

layout(binding = 0) uniform sampler2D sampler_texture;
in vec2 texcoord;

void main(){
	out_color = vec4(texture(sampler_texture, texcoord).xyz,1);
}