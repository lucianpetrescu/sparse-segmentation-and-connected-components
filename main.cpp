///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "core/renderer.h"
#include "algorithm_passthrough.h"
#include "algorithm_sparse_soft_connected_components.h"
#include <iomanip>

namespace sscc {
	class App {
		Renderer renderer;
		FileManager file_manager;
		AlgorithmInterface* algorithm = nullptr;
		GPUTimer timer{ 0 };
		struct Timing{
			float cpu =0;
			float gpu =0;
		};
		struct {
			std::vector<Timing> stages;
			Timing frame;
			float num_frames = 0;
			void reset() {
				num_frames = 0;
				frame.cpu = frame.gpu = 0;
				stages.clear();
			}
		}timings;
		
	public:

		///----------------------------------------------------------------------------------------------------------------------
		App(unsigned int width, unsigned int height) {
			//asset paths
			file_manager.setAssetPath("../input/");
			file_manager.setFile("noinput.png");
			renderer.setOutputPath("../output/");

			//renderer
			renderer.resize(width, height);
			algorithm = new AlgorithmSparseSoftConnectedComponents();
			std::cout << " -------------------------------------------------" << std::endl;
			std::cout << "Using algorithm <" << algorithm->getAlgorithmName() << ">"<< std::endl;
			std::cout << "Debug output is " << ((dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm))->accessOptions().debug ? "on" : "off") << std::endl;
		}
		~App() {
		}
		void render() {
			//set algorithm input
			algorithm->setInput(file_manager.getFileOutput(), &timer);

			//run algorithm
			algorithm->run();

			//average timings
			timer.synchronizeWithGPU();
			if (timer.getEntryCount() > 1) {
				if (timings.stages.size() != timer.getEntryCount()) {
					timings.reset();
					timings.stages.resize(timer.getEntryCount());
				}

				for (unsigned int i = 1; i < timer.getEntryCount(); i++) {
					timings.stages[i].cpu = (timings.stages[i].cpu * timings.num_frames + timer.getTimeBetweenEntriesInMillisecondsCPU(i-1,i)) / (timings.num_frames + 1);
					timings.stages[i].gpu = (timings.stages[i].gpu * timings.num_frames + timer.getTimeBetweenEntriesInMillisecondsGPU(i-1,i)) / (timings.num_frames + 1);
				}
				timings.frame.cpu = (timings.frame.cpu * timings.num_frames + timer.getTimeBetweenEntriesInMillisecondsCPU(0, timer.getEntryCount() - 1)) / (timings.num_frames + 1);
				timings.frame.gpu = (timings.frame.gpu * timings.num_frames + timer.getTimeBetweenEntriesInMillisecondsGPU(0, timer.getEntryCount() - 1)) / (timings.num_frames + 1);
			}
			if (timings.num_frames < 100000.f) timings.num_frames++;

			//present output
			renderer.setInput(algorithm->getOutput());
			renderer.render();
		}
		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			renderer.resize(width, height);
		}

		///----------------------------------------------------------------------------------------------------------------------
		void printResults() {
			//present results
			std::cout << " -------------------------------------------------" << std::endl;
			std::cout << " -------------------- RESULTS --------------------" << std::endl;
			std::cout << " The <" << algorithm->getAlgorithmName() << "> algorithm was timed over "<<(int)timings.num_frames<<" frames on input from "<<std::endl<<" <"<<file_manager.getFileOutput().filename<<"> , with the following measurements : " << std::endl;
			if (timings.stages.size() > 1) {
				for (int i = 1; i < timings.stages.size(); i++) std::cout << std::fixed<<std::setprecision(5) << " CPU avg = " << timings.stages[i].cpu << "ms\t GPU avg = " << timings.stages[i].gpu << " ms on stage <"<<timer.getNameEntry(i) << ">."<<std::endl;
				std::cout << std::fixed<< std::setprecision(5) << " Total execution times CPU = " << timings.frame.cpu << " ms   GPU = " << timings.frame.gpu << " ms" << std::endl;
			}
			else {
				std::cout << " No measurements" << std::endl;
			}
		}

		///----------------------------------------------------------------------------------------------------------------------
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			static bool window_resizeable = true;
			switch (key) {

			//debug
			case lap::wic::Key::SPACE:
				algorithm->debugReloadShaders(); 
				renderer.debugReloadShader();
			break;

			//close app
			case lap::wic::Key::ESCAPE: wnd.close();				break;	// the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed

			//pick algorithm
			case lap::wic::Key::NUM1:
				delete algorithm;
				algorithm = new AlgorithmSparseSoftConnectedComponents();
				timings.reset();
				std::cout << "Using algorithm " << algorithm->getAlgorithmName() << std::endl;
			break;
			case lap::wic::Key::NUM2:
				delete algorithm;
				algorithm = new AlgorithmPassthrough();
				timings.reset();
				std::cout << "Using algorithm " << algorithm->getAlgorithmName() << std::endl;
			break;

			//cycle renderer output
			case lap::wic::Key::DOWN: renderer.cycleTexturePrev();	break;
			case lap::wic::Key::UP: renderer.cycleTextureNext();break;

			//cycle file manager input and run
			case lap::wic::Key::LEFT:
				file_manager.cycleFilePrev();
				timings.reset();
				if (window_resizeable) wnd.setSize(std::max(file_manager.getWidth(),(unsigned int)100), std::max(file_manager.getHeight(), (unsigned int)100));
				break;
			case lap::wic::Key::RIGHT:
				file_manager.cycleFileNext();
				timings.reset();
				if(window_resizeable) wnd.setSize(std::max(file_manager.getWidth(), (unsigned int)100), std::max(file_manager.getHeight(), (unsigned int)100));
				break;
			case lap::wic::Key::P: printResults();	break;

				//various
			case lap::wic::Key::W: 
				window_resizeable = !window_resizeable;	
				std::cout << " --- window is now " << ((!window_resizeable) ? "not" : "") << " resizable." << std::endl;
				break;
			case lap::wic::Key::S : {
				static unsigned int index = 0;
				renderer.screenshot("out" + std::to_string(index) + ".png");	//saved in render output path + filename -> output/outINDEX.png
				index++;
			}	break;
			case lap::wic::Key::I: {
				static bool toggle = true;
				toggle = !toggle;
				if (toggle) renderer.setInputFilteringBilinear();
				else renderer.setInputFilteringNearest();
			}	break;
			case lap::wic::Key::H: {
				std::cout << std::endl << "----------------------------------------------------" << std::endl;
				std::cout << "----------------------- HELP -----------------------" << std::endl;
				std::cout << "GENERAL: " << std::endl;
				std::cout << "  1          - use sparse soft connected components" << std::endl;
				std::cout << "  2          - use pass through algorithm (does nothing)" << std::endl;
				std::cout << "  up down    - cycle through outputs (if debug is active it adds many outputs)" << std::endl;
				std::cout << "  left right - cycle through image files in the input folder (/input/)" << std::endl;
				std::cout << "  p          - print time measurements (per stage, per algorithm)" << std::endl;
				std::cout << "  esc        - close application" << std::endl;
				std::cout << "SSCC: " << std::endl;
				std::cout << "  - =        - decrease/increase maximum flux (edge)" << std::endl;
				std::cout << "  [ ]        - decrease/increase maximum accumulated flux per ray" << std::endl;
				std::cout << "  d          - enable/disable debug outputs" << std::endl;
				std::cout << "  f          - enable/disable full connected components" << std::endl;
				std::cout << "  : '        - decrease/increase tile size = decrease/increase number of seeds" << std::endl;
				std::cout << "  , .        - decrease/increase ray length" << std::endl;
				std::cout << "  z x        - decrease/increase number of region merging rounds" << std::endl;
				std::cout << "VARIOUS: " << std::endl;
				std::cout << "  space      - reload all GLSL shaders (useful for debug)" << std::endl;
				std::cout << "  w          - enables / disables window resize on file change" << std::endl;
				std::cout << "  s          - save a screenshot in the output folder (/output/)" << std::endl;
				std::cout << "  i          - toggle texture filtering in renderer between near and bilinear (useful on small windows..)" << std::endl;

			}	break;


			//threshold for edge / flux
			case lap::wic::Key::MINUS: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().threshold_flux = std::max(ss->accessOptions().threshold_flux - 0.001f, 0.001f);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using FLUX THRESHOLD " << ss->accessOptions().threshold_flux << std::endl;
				}
			}break;
			case lap::wic::Key::EQUAL: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss){
					ss->accessOptions().threshold_flux = std::min(ss->accessOptions().threshold_flux + 0.001f, 9999.9f);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using FLUX THRESHOLD " << ss->accessOptions().threshold_flux << std::endl;
				}
			}break;

			//threshold for accumulated flux (only for Sparse Segmentation)
			case lap::wic::Key::LEFT_BRACKET: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().threshold_accumulated_flux = std::max(ss->accessOptions().threshold_accumulated_flux - 0.01f, 0.2f);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using ACCUMULATED FLUX THRESHOLD " << ss->accessOptions().threshold_accumulated_flux << std::endl;
				}
			} break;
			case lap::wic::Key::RIGHT_BRACKET: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().threshold_accumulated_flux = std::min(ss->accessOptions().threshold_accumulated_flux + 0.01f, 5.0f);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using ACCUMULATED FLUX THRESHOLD " << ss->accessOptions().threshold_accumulated_flux << std::endl;
				}
			}break;

			//toggle debug output
			case lap::wic::Key::D: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss){
					ss->accessOptions().debug = !ss->accessOptions().debug;
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using DEBUG output " << (ss->accessOptions().debug ? "true" : "false") << std::endl;
				}
				timings.reset();
			}break;

			//toggle net full
			case lap::wic::Key::F: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().net_full = !ss->accessOptions().net_full;
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using NET FULL " << (ss->accessOptions().net_full ? "true" : "false") << std::endl;
				}
				timings.reset();
			} break;

			//increase / decrease tile size
			case lap::wic::Key::SEMICOLON: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().tile_size = std::max(ss->accessOptions().tile_size - 1, (unsigned int)4);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using TILE SIZE " << ss->accessOptions().tile_size << std::endl;
				}
				timings.reset();
			} break;
			case lap::wic::Key::APOSTROPHE: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().tile_size = std::min(ss->accessOptions().tile_size + 1, (unsigned int)200);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using TILE SIZE " << ss->accessOptions().tile_size << std::endl;
				}
				timings.reset();
			} break;
				
			//increase decrease ray length
			case lap::wic::Key::COMMA: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().ray_max_steps = std::max(ss->accessOptions().ray_max_steps - 1, (unsigned int)1);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using RAY MAX STEPS " << ss->accessOptions().ray_max_steps << std::endl;
				}
				timings.reset();
			}break;
			case lap::wic::Key::PERIOD:{
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().ray_max_steps = std::min(ss->accessOptions().ray_max_steps + 1, (unsigned int)300);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using RAY MAX STEPS " << ss->accessOptions().ray_max_steps << std::endl;
				}
				timings.reset();
			}break;
				//increase decrease ray length
			case lap::wic::Key::Z: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().region_merging_rounds = (unsigned int)std::max((int)ss->accessOptions().region_merging_rounds - 1, 0);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using REGION MERGING ROUNDS " << ss->accessOptions().region_merging_rounds << std::endl;
				}
				timings.reset();
			}break;
			case lap::wic::Key::X: {
				auto ss = dynamic_cast<AlgorithmSparseSoftConnectedComponents*>(algorithm);
				if (ss) {
					ss->accessOptions().region_merging_rounds = std::min(ss->accessOptions().region_merging_rounds + 1, (unsigned int)25);
					std::cout << "Algorithm " << algorithm->getAlgorithmName() << " using REGION MERGING ROUNDS " << ss->accessOptions().region_merging_rounds << std::endl;
				}
				timings.reset();
			}break;

			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION || type == GL_DEBUG_TYPE_OTHER) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cin.get();
	std::cout << std::endl;
}


int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "Sparse Soft Connected Components";						
	wp.width = 640;			wp.height = 480;
	wp.position_x = 1000;	wp.position_y = 200;
	fp.samples_per_pixel = 1;	//request a single sample per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);
	std::cout << std::endl << "----------------------------------------------" << std::endl;
	std::cout << std::endl << "----------------------------------------------" << std::endl;
	std::cout << std::endl << "----------------------------------------------" << std::endl;

	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//sscc App object + delegates
	sscc::App app(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&sscc::App::resize, &app, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&sscc::App::keyPress, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&sscc::App::keyRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&sscc::App::keyRepeat, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&sscc::App::mousePress, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&sscc::App::mouseRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&sscc::App::mouseDrag, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&sscc::App::mouseMove, &app, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&sscc::App::mouseScroll, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		app.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}