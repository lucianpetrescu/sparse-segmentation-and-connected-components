///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#pragma once
#include "core/gpu_texture.h"
#include "core/gpu_buffer.h"
#include "core/gpu_shader.h"
#include "core/gpu_timer.h"
#include "core/file_manager.h"
#include <vector>

namespace sscc {

	class AlgorithmInterface {
	public:

		virtual ~AlgorithmInterface() {}
		virtual void setInput(const FileOutput& input, GPUTimer* timer = nullptr) = 0;	//must be a non exponential input (depth from RGBD cameras needs a special flux function, this program is just an example)
		virtual void run() = 0;															//run algorithm
		virtual const std::vector<GPUTexture2D*>* getOutput() = 0;			
		virtual const std::string& getAlgorithmName() = 0;
		virtual void debugReloadShaders() = 0;
	};


}