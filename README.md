## SPARSE SOFT CONNECTED COMPONENTS ##

NOTE: this algorithm is in the process of being published.

![](img/0project.png)

This project implements a novel imperfect connected components algorithm, which is completely parallel and designed for GPUs. The algorithm grows and connects seeds by ray-tracing the input, thereby sparsely sampling a large domain while still computing an adequate coverage. The input can have any number of dimensions, thus the algorithm can be applied to images, 3D data, etc, in this project it is implemented for 2D images. On an N-sized input it can be implemented in O(steps) complexity per seed (gpu thread), where steps <<< N. The total complexity is O(seeds * steps), where seeds * steps </<< N.   

This algorithm is already used in production code for real-time video processing, where it imperfectly labels images in less then 5-10 ms on consumer laptop/mobile GPUs.

Compared to the state of the art methods, the sparse soft connected components works by generating seeds which are grown into regions with  ray tracing over a normalized gradient field (flux). The output is an image with sparsely labeled components. This sparse labeling is sufficient in many cases (imperfect segmentation, pre-processing for costly algorithms, dominant object detection, etc) and has been used with success on the depth signal of RGBD cameras. The algorithm also has an optional stage which fully labels the image and then does region merging, and both these stages are implemented completely on the GPU (0% CPU usage or delays).

All stages of the algorithm scale almost linearly with the number of pixels and further potential optimizations are now researched.

NOTE: this algorithm is parameter heavy and was designed for real-world cases.

###SPARSE SOFT CONNECTED COMPONENTS IN ACTION (VIDEOS) #
  
The algorithm on depth input from a RGBD camera.  
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXRmNxdld6bGdHdUE)](https://drive.google.com/open?id=0B3yTES0pEsWXUExoeFpJYk9xWTg "link to video").

Here is another video which shows how this algorithm is used as an input for best free space detection and for scene labeling.  
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXZ19KbkNzOWwwYlk)](https://drive.google.com/open?id=0B3yTES0pEsWXZlRROHZXNmY2UVk "link to video")

Here it a point cloud visualization of the input from the previous video.  
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXMG9rYzZvbG5hXzg)](https://drive.google.com/open?id=0B3yTES0pEsWXZnBjMHlUZ0ZDNFE "link to video").

#### ALGORITHM ####

The algorithm can function over any number of dimensions, in this implementation only images will be covered. 

The first stage is to compute the flux of the domain, this project uses a gradient normalized up to a threshold. The videos use more sophisticated methods. Example of flux:

![](img/peppers1.png) 

A number of seeds is generated over the entire image. The number can be optimized, depending on the type of the expected input. The distribution of the seeds is pseudorandom over the entire domain (Poisson Disk sampling is used). The following images show the transition from few to many seeds :  
![](img/seed0.png)  
![](img/seed1.png)  
![](img/seed2.png)  
![](img/seed3.png)  

After the seeds are generated ray-tracing is used from each seed, making each region use few samples to cover nearby space. When rays intersect the seeds connect in a tree structure, thus a forest is constructed over the entire domain. Rays finish when going out of bounds, when intersecting a ray from the same tree or when they have accumulated too much flux. 

When each seed finishes covering its vicinity and connecting to potential neighbors the final result is a forest of ids, each with a vicinity, covering the entire domain with a net. The density of the net depends on the number of the seeds and on the number of steps per seed, examples:

![](img/tile0.png) ![](img/tile1.png) ![](img/tile2.png) ![](img/tile3.png)  

The next stages are optional. The first one expands the sparse coverage to full coverage by using per-input element kernels (e.g. 2D kernel per pixel for images). With the same ray tracing rules used in the net creation stage, each un-labeled input is connected to the net. This is an iterative process, but with the proper parameters a very small (2-3) number of iterations is sufficient. Examples:

![](img/house3.png)![](img/house4.png)  
![](img/synthetic3.png)![](img/synthetic4.png)  

The second optional step performs GPU region merging. It first computes stats per each region and then tries to connect the regions using these stats and the local flux. Example of filled vs sparse:

![](img/full-no.png) ![](img/full-yes.png)  


#### SUPERPIXELS?  ####

If the flux per ray is set to a small value then the algorithm works as an imperfect [superpixels](http://docs.opencv.org/3.0-beta/modules/ximgproc/doc/superpixels.html) method, creating many clusters which can then be merged into regions. Example:

![](img/superpixel0.png) ![](img/superpixel1.png) 

### SCREENSHOTS ###

| Original Image / Sparse Labeling | Flux / Full Labeling | Seeds / Region Merged Labeling|
| --- | --- | --- |
| ![](img/cameraman0.png)      | ![](img/cameraman1.png)| ![](img/cameraman2.png) |
| ![](img/cameraman3.png)      | ![](img/cameraman4.png)| ![](img/cameraman5.png) |
|  |  |
| ![](img/circle0.png)      | ![](img/circle1.png)| ![](img/circle2.png) |
| ![](img/circle3.png)      | ![](img/circle4.png)| ![](img/circle5.png) |
|  |  |
| ![](img/hard0.png)      | ![](img/hard1.png)| ![](img/hard2.png) |
| ![](img/hard3.png)      | ![](img/hard4.png)| ![](img/hard5.png) |
|  |  |
| ![](img/house0.png)      | ![](img/house1.png)| ![](img/house2.png) |
| ![](img/house3.png)      | ![](img/house4.png)| ![](img/house5.png) |  
|  |  |
| ![](img/peppers0.png)      | ![](img/peppers1.png)| ![](img/peppers2.png) |
| ![](img/peppers3.png)      | ![](img/peppers4.png)| ![](img/peppers5.png) |
|  |  |
| ![](img/synthetic0.png)      | ![](img/synthetic1.png)| ![](img/synthetic2.png) |
| ![](img/synthetic3.png)      | ![](img/synthetic4.png)| ![](img/synthetic5.png) |
|  |  |
| ![](img/text0.png)      | ![](img/text1.png)| ![](img/text2.png) |
| ![](img/text3.png)      | ![](img/text4.png)| ![](img/text5.png) |

#### BUILDING and DEPENDENCIES ####

This project uses [lap_wic](https://bitbucket.org/lucianpetrescu/public) for all OpenGL context, input handling and windowing needs and [glm](http://glm.g-truc.net/0.9.8/index.html) for mathematics. The project should compile with any CPP11 compiler. A 2015 visual studio project is prodived in the /vstudio folder. Makefiles for visual studio (makefilevs.bat) and gcc (linux, macos) are found in the root folder. 

Tested under Windows 7/10, under various NVIDIA GPUs.  

Notes:  
- a 4.5 OpenGL compliant driver is required.  
- apparently there is (was?) a problem under AMD GPUs, investigating, probably a driver issue.   
- this project uses GPU lockless synchronization, thus tinkering with some of the GLSL code might lead to total system breakdowns or a serious slowdown.  

#### LICENSE ####

The MIT License (MIT)
 
Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.