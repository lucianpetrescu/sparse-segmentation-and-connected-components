///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "algorithm_sparse_soft_connected_components.h"

namespace sscc{


	//GPU region contents
	class GPUSegmentationRegion {
	public:
		int seedx = 0;			//seed start x
		int seedy = 0;			//seed start y
		int id = 0;			//id
		int parentid = 0;		//parent id
	};

	//stats for GPU region merging
	class GPUSegmentationRegionStats {
		int r, g, b;			//needed for accumulation of atomic operations, otherwise stuck only on NVIDIA
		int samples;
	};



	AlgorithmSparseSoftConnectedComponents::AlgorithmSparseSoftConnectedComponents() {
		//sampler
		sampler.setNearest();
		sampler.setClamp();

		//shaders
		shader_init_regions.load("../shaders/sparse_soft_cc_init_regions.comp");
		shader_init_seeds.load("../shaders/sparse_soft_cc_init_seeds.comp");
		shader_net_create.load("../shaders/sparse_soft_cc_net_create.comp");
		shader_net_link.load("../shaders/sparse_soft_cc_net_link.comp");
		shader_net_link_region_connect.load("../shaders/sparse_soft_cc_net_link_region_connect.comp");
		shader_net_link_region_stats.load("../shaders/sparse_soft_cc_net_link_region_stats.comp");
		shader_net_overlap_solver.load("../shaders/sparse_soft_cc_net_overlap_solver.comp");
		shader_net_update.load("../shaders/sparse_soft_cc_net_update.comp");
		shader_colorize.load("../shaders/sparse_soft_cc_colorize.comp");
	}
	AlgorithmSparseSoftConnectedComponents::~AlgorithmSparseSoftConnectedComponents() {
	}

	void AlgorithmSparseSoftConnectedComponents::setInput(const FileOutput& in_input, GPUTimer* timer) {
		//edge detector
		edge_detector.setInput(in_input.gpudata, in_input.channels);

		//timer
		gpu_timer = timer;
		gpu_timer->resize(6);

		//resize?
		if (in_input.width != input.width || in_input.height != input.height) {
			
			//sparse segmentation
			num_seeds = in_input.width * in_input.height / options.tile_size / options.tile_size;

			gpu_tex_regions.create(GL_R32I, in_input.width, in_input.height, false);
			gpu_tex_regions2.create(GL_R32I, in_input.width, in_input.height, false);
			gpu_buf_regions_data.resize(sizeof(GPUSegmentationRegion) * num_seeds);
			gpu_buf_regions_merging_stats.resize(sizeof(GPUSegmentationRegionStats) * num_seeds);

			debug_seeds_only.create(GL_RGBA8, in_input.width, in_input.height, false);
			debug_net_only.create(GL_RGBA8, in_input.width, in_input.height, false);
			debug_net_only.create(GL_RGBA8, in_input.width, in_input.height, false);
			debug_net_without_merging.create(GL_RGBA8, in_input.width, in_input.height, false);
			result.create(GL_RGBA8, in_input.width, in_input.height, false);
		}

		//update input
		input = in_input;
	}
	void AlgorithmSparseSoftConnectedComponents::run() {
		//update tile size and num_seeds
		int old_num_seeds = (int)gpu_buf_regions_data.getSize() / sizeof(GPUSegmentationRegion);
		num_seeds = input.width * input.height / options.tile_size / options.tile_size;
		if (old_num_seeds != num_seeds) {
			gpu_buf_regions_data.resize(sizeof(GPUSegmentationRegion) * num_seeds);
			gpu_buf_regions_merging_stats.resize(sizeof(GPUSegmentationRegionStats) * num_seeds);
		}


		//compute flux
		gpu_timer->insertEntry(0, "start");
		edge_detector.computeFlux(true, options.threshold_flux);
		gpu_timer->insertEntry(1, "flux");
		
		//-----------------------------------------------------------------------------------------------------------
		//												INITIALIZATION 
		//-----------------------------------------------------------------------------------------------------------
		//initialize regions (set each pixel to region NO_ID)
		shader_init_regions.bind();
		shader_init_regions.setUniform("num_seeds", (int)num_seeds);
		gpu_tex_regions.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R32I);
		glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		// initialize seeds (zero out each region and pick a randomlu/poisson/uniformly distributed position for the each region seed)
		// seed = ( poisson random pos, 0,0,0,0.. )
		shader_init_seeds.bind();
		shader_init_seeds.setUniform("num_seeds", (int)num_seeds);
		shader_init_seeds.setUniform("TILE_SIZE", (int)options.tile_size);
		shader_init_seeds.setUniform("DEBUG",(int) options.debug);
		gpu_tex_regions.bindAsImage(4, GL_WRITE_ONLY, 0, GL_R32I);
		edge_detector.getOutputMagnitude()->bindAsImage(2, GL_READ_ONLY, 0, GL_R8);
		gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
		glDispatchCompute(int(num_seeds + 31) / 32, 1, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		gpu_timer->insertEntry(2, "initialization");

		//debug
		if (options.debug) {
			shader_colorize.bind();
			shader_colorize.setUniform("num_seeds", (int)num_seeds);
			gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
			debug_seeds_only.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
			glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}

		//-----------------------------------------------------------------------------------------------------------
		//														NET 
		//-----------------------------------------------------------------------------------------------------------
		// create net (trace rays from each seed and connect the rays into an image spanning net, to which the other pixels will connect)
		// RAY CASTING IN 2D
		//   \    -------\           /\
		//    \  /        \         /  \
		//     \/------------------/    \
		//     /         /               \
		//    /         /
		// creates regions, but not segments
		shader_net_create.bind();
		shader_net_create.setUniform("NUM_SEEDS", (int)num_seeds);
		shader_net_create.setUniform("MAX_FLUX", options.threshold_accumulated_flux);
		shader_net_create.setUniform("MAX_STEPS", (int)options.ray_max_steps);
		gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
		edge_detector.getOutputMagnitude()->bindAsImage(0, GL_READ_ONLY, 0, GL_R8);
		edge_detector.getOutputAngle()->bindAsImage(1, GL_READ_ONLY, 0, GL_R8);
		gpu_tex_regions.bindAsImage(2, GL_READ_WRITE, 0, GL_R32I);						//improved parallellism, basically tracing rays instead of growing vicinities
		glDispatchCompute((int(num_seeds + 31) / 32) * 8, 1, 1);						//the number of seeds * number of directions (hardcoded)
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		//overlap solver (e.g. 2 rays finish near each other but never overlap)
		shader_net_overlap_solver.bind();
		gpu_tex_regions.bindAsImage(0, GL_READ_ONLY, 0, GL_R32I);//readonly
		edge_detector.getOutputMagnitude()->bindAsImage(1, GL_READ_ONLY, 0, GL_R8);
		gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
		shader_net_overlap_solver.setUniform("num_seeds", (int)num_seeds);
		glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		// now that the regions are connected update the net id at each pixel to the greatest parent 
		shader_net_update.bind();
		shader_net_update.setUniform("num_seeds", (int)num_seeds);
		gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
		gpu_tex_regions.bindAsImage(0, GL_READ_WRITE, 0, GL_R32I);
		glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		gpu_timer->insertEntry(3, "net creation");

		//debug
		if (options.debug) {
			shader_colorize.bind();
			shader_colorize.setUniform("num_seeds", (int)num_seeds);
			gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
			debug_net_only.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
			glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}


		if (options.net_full) {
			//-----------------------------------------------------------------------------------------------------------
			//											LINK TO NET 
			//-----------------------------------------------------------------------------------------------------------
			// link net (connect the other pixels to the net)
			//
			//  stage 1. connect following normal connection rules
			//			for each pixel (not in a region, not edge = max flux) sample neighboring area with rays
			//			which end when enough flow was accumulated. If the ray hits a region, then connect 
			//			the filtered pixel to that region.
			//  stage 2. connect non edges unconnected at stage 1
			//			for each pixel (not in a region, not edge = max flux) sample neighboring area with rays which
			//			never end but always sample normal + depth for pixels in grown regions. At the end
			//			the filtered pixel is assigned to the best fit
			//  stage 3. connect anything left, including edges
			//			basically stage 2 again, this time edges (max flux pixels) are also filtered.
			//  stage 4. 
			//			region merging
			const int LINK_STAGE_CONNECT = 1;
			const int LINK_STAGE_UNCONNECTABLES = 2;
			const int LINK_STAGE_ANYTHING = 3;

			//uniforms
			shader_net_link.bind();
			shader_net_link.setUniform("NUM_SEEDS", (int)num_seeds);
			shader_net_link.setUniform("MAX_FLUX", options.threshold_accumulated_flux);
			edge_detector.getOutputMagnitude()->bindAsImage(0, GL_READ_ONLY, 0, GL_R8);

			////////////////////////////////////////////////////////////////////////////////
			//STAGE 1 (4 rounds) - connect pixels to the net exactly
			shader_net_link.setUniform("LINK_STAGE", 1);
			for (int i = 0; i < 4; i++) {
				gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
				gpu_tex_regions2.bindAsImage(5, GL_WRITE_ONLY, 0, GL_R32I);
				glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
				gpu_tex_regions2.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
				gpu_tex_regions.bindAsImage(5, GL_WRITE_ONLY, 0, GL_R32I);
				glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
			}

			///////////////////////////////////////////////////////////////////////////////
			//STAGE 2 - link pixels by depth (and a weak normal test). Should solve most of
			//			the unlinked and edge pixels.
			shader_net_link.setUniform("LINK_STAGE", 2);
			gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
			gpu_tex_regions2.bindAsImage(5, GL_WRITE_ONLY, 0, GL_R32I);
			glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);


			////////////////////////////////////////////////////////////////////////////////
			//STAGE 3 - link the edges
			shader_net_link.bind();
			shader_net_link.setUniform("LINK_STAGE", 3);
			sampler.bind(0);
			input.gpudata->bindAsTexture(0);
			edge_detector.getOutputMagnitude()->bindAsImage(0, GL_READ_ONLY, 0, GL_R8);
			gpu_tex_regions2.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
			gpu_tex_regions.bindAsImage(5, GL_WRITE_ONLY, 0, GL_R32I);
			glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);

			gpu_timer->insertEntry(4, "full linking");

			//debug
			if (options.debug) {
				shader_colorize.bind();
				shader_colorize.setUniform("num_seeds", (int)num_seeds);
				gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
				debug_net_without_merging.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
				glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
			}

			////////////////////////////////////////////////////////////////////////////////
			//STAGE 4 (OPTIONAL) - connect the regions  
			//						This is a complex multipass stage in which stats are
			//						computed per region and then the regions are linked.
			// progressive region growing
			for (unsigned int pass = 0; pass < options.region_merging_rounds; pass++) {
				// compute the average stats per region
				shader_net_link_region_stats.bind();
				shader_net_link_region_stats.setUniform("num_seeds", (int)num_seeds);
				gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
				gpu_buf_regions_merging_stats.bindIndexed(GL_SHADER_STORAGE_BUFFER, 1);
				sampler.bind(0);
				input.gpudata->bindAsTexture(0);
				gpu_tex_regions.bindAsImage(0, GL_READ_ONLY, 0 , GL_R32I);
				edge_detector.getOutputMagnitude()->bindAsImage(1, GL_READ_ONLY, 0, GL_R8);
				shader_net_link_region_stats.setUniform("PASS", 1);		//initialize stats
				glDispatchCompute(int(num_seeds + 31) / 32, 1, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
				shader_net_link_region_stats.setUniform("PASS", 2);		//accumulate stats
				glDispatchCompute(int(num_seeds + 31) / 32, 1, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
				shader_net_link_region_stats.setUniform("PASS", 3);		//finalize stats
				glDispatchCompute(int(num_seeds + 31) / 32, 1, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);

				// connect based on stats (O (pixel))
				shader_net_link_region_connect.bind();
				sampler.bind(0);
				input.gpudata->bindAsTexture(0);
				shader_net_link_region_connect.setUniform("LENIENCY", ((pass+1) / (float)options.region_merging_rounds));
				shader_net_link_region_connect.setUniform("num_seeds", (int)num_seeds);
				shader_net_link_region_connect.setUniform("THRESHOLD", options.threshold_flux);
				gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
				gpu_buf_regions_merging_stats.bindIndexed(GL_SHADER_STORAGE_BUFFER, 1);
				gpu_tex_regions.bindAsImage(0, GL_READ_ONLY, 0, GL_R32I);
				edge_detector.getOutputMagnitude()->bindAsImage(1, GL_READ_ONLY, 0, GL_R8);
				glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
				
				//update pixel ids
				shader_net_update.bind();
				gpu_buf_regions_data.bindIndexed(GL_SHADER_STORAGE_BUFFER, 0);
				gpu_tex_regions.bindAsImage(0, GL_READ_WRITE, 0, GL_R32I);
				glDispatchCompute(int(input.width + 31) / 32, int(input.height + 31) / 32, 1);
				glMemoryBarrier(GL_ALL_BARRIER_BITS);
			}
		}else {
			gpu_timer->insertEntry(4, "full linking");
		}

		gpu_timer->insertEntry(5, "region merging");


		shader_colorize.bind();
		shader_colorize.setUniform("num_seeds", (int)num_seeds);
		gpu_tex_regions.bindAsImage(4, GL_READ_ONLY, 0, GL_R32I);
		result.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
		glDispatchCompute((input.width + 7) / 8, (input.height + 7) / 8, 1);	//8x8 kernels
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}
	const std::vector<GPUTexture2D*>* AlgorithmSparseSoftConnectedComponents::getOutput() {
		output.clear();
		output.push_back(input.gpudata);
		if (options.debug) {
			output.push_back(edge_detector.getOutputMagnitude());
			output.push_back(edge_detector.getOutputAngle());
			output.push_back(&debug_seeds_only);
			output.push_back(&debug_net_only);
			output.push_back(&debug_net_without_merging);
		}
		output.push_back(&result);
		return &output;
	}
	const std::string& AlgorithmSparseSoftConnectedComponents::getAlgorithmName() {
		return algoritm_name;
	}


	AlgorithmSparseSoftConnectedComponents::Options& AlgorithmSparseSoftConnectedComponents::accessOptions() {
		return options;
	}


	void AlgorithmSparseSoftConnectedComponents::debugReloadShaders() {
		//edge
		edge_detector.debugReloadShaders();

		//shaders
		shader_init_regions.reload();
		shader_init_seeds.reload();
		shader_net_create.reload();
		shader_net_link.reload();
		shader_net_link_region_connect.reload();
		shader_net_link_region_stats.reload();
		shader_net_overlap_solver.reload();
		shader_net_update.reload();
		shader_colorize.reload();
	}

}