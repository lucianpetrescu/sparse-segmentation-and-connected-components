///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
#include "algorithm_interface.h"
#include "core/gpu_edge_detection.h"

namespace sscc{

	class AlgorithmSparseSoftConnectedComponents : public AlgorithmInterface{
	public:
		AlgorithmSparseSoftConnectedComponents();
		AlgorithmSparseSoftConnectedComponents(const AlgorithmSparseSoftConnectedComponents&) = delete;
		AlgorithmSparseSoftConnectedComponents(AlgorithmSparseSoftConnectedComponents&&) = delete;
		AlgorithmSparseSoftConnectedComponents& operator=(const AlgorithmSparseSoftConnectedComponents&) = delete;
		AlgorithmSparseSoftConnectedComponents& operator=(AlgorithmSparseSoftConnectedComponents&&) = delete;
		~AlgorithmSparseSoftConnectedComponents();

		//input
		void setInput(const FileOutput& input, GPUTimer* timer = nullptr);

		//options
		struct Options {
			bool debug = true;
			float threshold_flux = 0.3f;
			float threshold_accumulated_flux = 1.2f;
			bool net_full = true;
			unsigned int tile_size = 12;				// should NEVER be smaller than 4 (will affect some optimized compression on bandwidth)
			unsigned int ray_max_steps = 100;			// the number of steps taken while tracing
			unsigned int region_merging_rounds = 3;		// region merging rounds
		};

		//options
		Options& accessOptions();

		//run & output
		void run();
		const std::vector<GPUTexture2D*>* getOutput();
		const std::string& getAlgorithmName();

		//debug
		void debugReloadShaders();
	private:
		//interface
		const std::string algoritm_name = "Algorithm Sparse Soft Connected Components";
		FileOutput input;
		std::vector<GPUTexture2D*> output;
		GPUTimer* gpu_timer;

		//options
		Options options;

		//output
		GPUTexture2D debug_seeds_only;
		GPUTexture2D debug_net_only;
		GPUTexture2D debug_net_without_merging;
		GPUTexture2D result;

		///---------------------------- CORE ----------------------------
		//flux
		GPUEdgeDetector edge_detector;

		//algorithm
		int num_seeds;
		GPUTextureSampler sampler;
		GPUTexture2D gpu_tex_regions;
		GPUTexture2D gpu_tex_regions2;
		GPUBuffer gpu_buf_regions_data;
		GPUBuffer gpu_buf_regions_merging_stats;
		GPUShader shader_init_seeds;
		GPUShader shader_init_regions;
		GPUShader shader_net_create;
		GPUShader shader_net_overlap_solver;
		GPUShader shader_net_link_region_connect;
		GPUShader shader_net_link_region_stats;
		GPUShader shader_net_link;
		GPUShader shader_net_update;
		GPUShader shader_colorize;
	};
}