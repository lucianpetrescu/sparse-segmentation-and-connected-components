///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "gpu_edge_detection.h"

namespace sscc{
	GPUEdgeDetector::GPUEdgeDetector() {
		//samplers
		sampler_near.setClamp();
		sampler_near.setNearest();
		sampler_bilinear.setClamp();
		sampler_bilinear.setBilinear();

		//gaussian blurring
		shader_blur.load("../shaders/edge_gaussian_blur.comp");
		shader_edge.load("../shaders/edge_detection.comp");
		gpu_tex1.create(GL_R8, 10, 10, false);
		gpu_tex2.create(GL_R8, 10, 10, false);
		gpu_tex3.create(GL_R8, 10, 10, false);
	}
	GPUEdgeDetector::~GPUEdgeDetector() {
	}

	void GPUEdgeDetector::setInput(GPUTexture2D* in_input, int in_num_channels) {
		//input
		input = in_input;
		num_channels = in_num_channels;

		//resize?
		unsigned int w = input->getWidth();
		unsigned int h = input->getHeight();
		if (w != gpu_tex1.getWidth() || h != gpu_tex2.getHeight()) {
			gpu_tex1.create(GL_R8, w, h, false);
			gpu_tex2.create(GL_R8, w, h, false);
			gpu_tex3.create(GL_R8, w, h, false);
		}
	}
	void GPUEdgeDetector::computeEdge(bool blur, float threshold_upper, float threshold_lower, int hysterezis_iterations) {
		unsigned int w = input->getWidth();
		unsigned int h = input->getHeight();

		if (blur) {
			//gaussian blur
			sampler_bilinear.bind(0);
			shader_blur.bind();
			shader_blur.setUniform("CHANNELS", (int)num_channels);
			shader_blur.setUniform("PASS", 1);
			input->bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			shader_blur.setUniform("PASS", 2);
			gpu_tex1.bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
		else {
			//passthrough
			sampler_near.bind(0);
			shader_blur.bind();
			shader_blur.setUniform("CHANNELS", (int)num_channels);
			shader_blur.setUniform("PASS", 0);
			input->bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}

		//edge detect gradient
		sampler_near.bind(0);
		sampler_near.bind(1);
		shader_edge.bind();
		shader_edge.setUniform("THRESHOLD_UPPER", threshold_upper);
		shader_edge.setUniform("THRESHOLD_LOWER", threshold_lower);
		shader_edge.setUniform("PASS", 1);
		gpu_tex3.bindAsTexture(0);
		gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
		gpu_tex2.bindAsImage(1, GL_WRITE_ONLY, 0, GL_R8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		//non maximal suppression
		shader_edge.setUniform("PASS", 2);
		gpu_tex1.bindAsTexture(0);
		gpu_tex2.bindAsTexture(1);
		gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		//hysteresis thresholding (4 passes)
		for (int i = 0; i < hysterezis_iterations; i++) {
			shader_edge.setUniform("PASS", 3);
			gpu_tex3.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			gpu_tex1.bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
	}
	void GPUEdgeDetector::computeFlux(bool blur, float threshold_upper) {
		unsigned int w = input->getWidth();
		unsigned int h = input->getHeight();

		if (blur) {
			//gaussian blur
			sampler_bilinear.bind(0);
			shader_blur.bind();
			shader_blur.setUniform("CHANNELS", (int)num_channels);
			shader_blur.setUniform("PASS", 1);
			input->bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			shader_blur.setUniform("PASS", 2);
			gpu_tex3.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
		else {
			//passthrough
			sampler_near.bind(0);
			shader_blur.bind();
			shader_blur.setUniform("CHANNELS", (int)num_channels);
			shader_blur.setUniform("PASS", 0);
			input->bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}

		//flux gradient
		sampler_near.bind(0);
		sampler_near.bind(1);
		shader_edge.bind();
		shader_edge.setUniform("THRESHOLD_UPPER", threshold_upper);
		shader_edge.setUniform("PASS", 4);
		gpu_tex1.bindAsTexture(0);
		gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
		gpu_tex2.bindAsImage(1, GL_WRITE_ONLY, 0, GL_R8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}
	GPUTexture2D* GPUEdgeDetector::getOutputMagnitude() {
		return &gpu_tex3;
	}
	GPUTexture2D* GPUEdgeDetector::getOutputAngle() {
		return &gpu_tex2;
	}
	void GPUEdgeDetector::debugReloadShaders() {
		shader_blur.reload();
		shader_edge.reload();
	}

}
