///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "gpu_texture.h"
#include "gpu_shader.h"

namespace sscc{

	class GPUEdgeDetector {
	public:
		GPUEdgeDetector();
		~GPUEdgeDetector();

		void setInput(GPUTexture2D* input, int num_channels);
		void computeEdge(bool blur, float threshold_upper, float threshold_lower, int hysterezis_iterations);
		void computeFlux(bool blur, float threshold_upper);
		GPUTexture2D* getOutputMagnitude();
		GPUTexture2D* getOutputAngle();
		void debugReloadShaders();
	private:
		int num_channels = 3;
		GPUTexture2D* input;
		GPUTextureSampler sampler_near, sampler_bilinear;

		GPUShader shader_blur;
		GPUShader shader_edge;
		GPUTexture2D gpu_tex1;
		GPUTexture2D gpu_tex2;
		GPUTexture2D gpu_tex3;
	};
}
