///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "algorithm_passthrough.h"

namespace sscc{

	AlgorithmPassthrough::AlgorithmPassthrough() {
	}
	AlgorithmPassthrough::~AlgorithmPassthrough() {
	}

	void AlgorithmPassthrough::setInput(const FileOutput& in_input, GPUTimer* timer){
		output.clear();
		output.push_back(in_input.gpudata);		//just do a pass through on the input
		gputimer = timer;
		gputimer->resize(2);
	}
	void AlgorithmPassthrough::run() {
		gputimer->insertEntry(0, "start");
		gputimer->insertEntry(1, "finish");
	}
	const std::vector<GPUTexture2D*>* AlgorithmPassthrough::getOutput() {
		return &output;
	}
	const std::string& AlgorithmPassthrough::getAlgorithmName() {
		return algoritm_name;
	}
	void AlgorithmPassthrough::debugReloadShaders() {
		//nothing
	}
}